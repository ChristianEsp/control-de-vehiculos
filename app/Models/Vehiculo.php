<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vehiculo
 *
 * @property $id
 * @property $placas
 * @property $marca
 * @property $modelo
 * @property $color
 * @property $tipo_combustible
 * @property $fecha_adquisicion
 * @property $estatus
 * @property $user_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Vehiculo extends Model
{
    
    static $rules = [
		'placas' => 'required',
		'marca' => 'required',
		'modelo' => 'required',
		'tipo_combustible' => 'required',
		'fecha_adquisicion' => 'required',
		'estatus' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['placas','marca','modelo','color','tipo_combustible','fecha_adquisicion','estatus','user_id'];



}
