<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Vehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Vehiculos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('placas');
            $table->string('marca');
            $table->string('modelo');
            $table->string('color');
            $table->tinyInteger('tipo_combustible');
            $table->datetime('fecha_adquisicion');
            $table->tinyInteger('estatus')->default(1)->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
