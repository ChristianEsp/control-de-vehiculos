@extends('layouts.app')

@section('template_title')
    {{ $vehiculo->name ?? 'Show Vehiculo' }}
@endsection

@section('content')
    <section class="content container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Evaluación del Vehículo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('vehiculos.index') }}"> Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Placas:</strong>
                            {{ $vehiculo->placas }}
                        </div>
                        <div class="form-group">
                            <strong>Marca:</strong>
                            {{ $vehiculo->marca }}
                        </div>
                        <div class="form-group">
                            <strong>Modelo:</strong>
                            {{ $vehiculo->modelo }}
                        </div>
                        <div class="form-group">
                            <strong>Color:</strong>
                            {{ $vehiculo->color }}
                        </div>
                        <div class="form-group">
                            <strong>Tipo de Combustible:</strong>
                            {{ $vehiculo->tipo_combustible }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha de Adquisición:</strong>
                            {{ $vehiculo->fecha_adquisicion }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha de Alta:</strong>
                            {{ $vehiculo->created_at }}
                        </div>
                        <div class="form-group">
                            @if ($vehiculo->estatus == 1)
                            <strong>Estatus: </strong><span class="badge badge-primary">Enviado</span>
                            @elseif ($vehiculo->estatus == 2)
                            <strong>Estatus: </strong><span class="badge badge-light">Asignado</span>
                            @elseif ($vehiculo->estatus == 3)
                            <strong>Estatus: </strong><span class="badge badge-success">Disponible</span>
                            @else
                            <strong>Estatus: </strong><span class="badge badge-danger">Baja</span>
                            @endif
                        </div>
                        @if ($vehiculo->estatus == 1 or $vehiculo->estatus == 3)
                            <form method="POST" action="{{ route('vehiculos.update', $vehiculo->id) }}"  role="form" enctype="multipart/form-data">    
                                <div class="form-group">
                                    <strong>Asignar a Empleado</strong>
                                    <select name="user_id" >
                                        <option value="0">Elige un empleado</option>
                                        @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <input name="placas" type="hidden" value="{{ $vehiculo->placas }}">
                                    <input name="marca" type="hidden" value="{{ $vehiculo->marca }}">
                                    <input name="modelo" type="hidden" value="{{ $vehiculo->modelo }}">
                                    <input name="color" type="hidden" value="{{ $vehiculo->color }}">
                                    <input name="fecha_adquisicion" type="hidden" value="{{ $vehiculo->fecha_adquisicion }}">
                                    <input name="tipo_combustible" type="hidden" value="{{ $vehiculo->tipo_combustible }}">
                                    <input name="estatus" type="hidden" value="2">
                                    <button type="submit" class="btn btn-success">Guardar</button>
                                </div>
                            </form>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
