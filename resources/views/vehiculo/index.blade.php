@extends('layouts.app')

@section('template_title')
    Vehiculo
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Vehículos') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('vehiculos.create') }}" class="btn btn-success btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
										<th>Placas</th>
										<th>Marca</th>
										<th>Modelo</th>
                                        <th>Estatus</th>

                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vehiculos as $vehiculo)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $vehiculo->placas }}</td>
											<td>{{ $vehiculo->marca }}</td>
											<td>{{ $vehiculo->modelo }}</td>
                                            @if ($vehiculo->estatus == 1)
											    <td><span class="badge badge-primary">Enviado</span></td>
                                            @elseif ($vehiculo->estatus == 2)
                                                <td><span class="badge badge-light">Asignado</span></td>
                                            @elseif ($vehiculo->estatus == 3)
                                                <td><span class="badge badge-success">Disponible</span></td>
                                            @else
                                                <td><span class="badge badge-danger">Baja</span></td>
                                            @endif
                                            <td>
                                                <form action="{{ route('vehiculos.destroy',$vehiculo->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-warning" href="{{ route('vehiculos.edit',$vehiculo->id) }}"><i class="fa fa-fw fa-eye"></i> Evaluación</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Eliminar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $vehiculos->links() !!}
            </div>
        </div>
    </div>
@endsection
