<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('placas') }}
            {{ Form::text('placas', $vehiculo->placas, ['class' => 'form-control' . ($errors->has('placas') ? ' is-invalid' : ''), 'placeholder' => 'Placas']) }}
            {!! $errors->first('placas', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('marca') }}
            {{ Form::text('marca', $vehiculo->marca, ['class' => 'form-control' . ($errors->has('marca') ? ' is-invalid' : ''), 'placeholder' => 'Marca']) }}
            {!! $errors->first('marca', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('modelo') }}
            {{ Form::text('modelo', $vehiculo->modelo, ['class' => 'form-control' . ($errors->has('modelo') ? ' is-invalid' : ''), 'placeholder' => 'Modelo']) }}
            {!! $errors->first('modelo', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('color') }}
            {{ Form::text('color', $vehiculo->color, ['class' => 'form-control' . ($errors->has('color') ? ' is-invalid' : ''), 'placeholder' => 'Color']) }}
            {!! $errors->first('color', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label>Tipo de Combustible</label>
            <select class="form-control" placeholder="Color" name="tipo_combustible">
                <option value="1">Gasolina</option>
                <option value="2">Diesel</option>
            </select>
        </div>
        <div class="form-group">
            <label>Fecha de Adquisición</label>
            {{ Form::date('fecha_adquisicion', $vehiculo->fecha_adquisicion, ['class' => 'form-control' . ($errors->has('fecha_adquisicion') ? ' is-invalid' : ''), 'placeholder' => 'Fecha de Adquisición']) }}
            {!! $errors->first('fecha_adquisicion', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <input type="hidden" name="estatus" value="1">
        </div>
    </div>
    <div class="box-footer mt20 float-right">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('vehiculos.index') }}" class="btn btn-light"  data-placement="left">Cancelar</a>
    </div>
</div>