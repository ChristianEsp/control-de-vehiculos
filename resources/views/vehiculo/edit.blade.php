@extends('layouts.app')

@section('template_title')
    Update Vehiculo
@endsection

@section('content')
    <section class="content container">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title"><b>Evaluación del Vehículo</b></span>
                    </div>
                    <div class="card-header">
                        <label id="asignacion"></label>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('vehiculos.update', $vehiculo->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf


                            <div class="box box-info padding-1">
                                <div class="box-body">
                                    
                                    <div class="form-group">
                                        {{ Form::label('placas') }}
                                        {{ Form::text('placas', $vehiculo->placas, ['class' => 'form-control' . ($errors->has('placas') ? ' is-invalid' : ''), 'placeholder' => 'Placas']) }}
                                        {!! $errors->first('placas', '<div class="invalid-feedback">:message</p>') !!}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('marca') }}
                                        {{ Form::text('marca', $vehiculo->marca, ['class' => 'form-control' . ($errors->has('marca') ? ' is-invalid' : ''), 'placeholder' => 'Marca']) }}
                                        {!! $errors->first('marca', '<div class="invalid-feedback">:message</p>') !!}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('modelo') }}
                                        {{ Form::text('modelo', $vehiculo->modelo, ['class' => 'form-control' . ($errors->has('modelo') ? ' is-invalid' : ''), 'placeholder' => 'Modelo']) }}
                                        {!! $errors->first('modelo', '<div class="invalid-feedback">:message</p>') !!}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('color') }}
                                        {{ Form::text('color', $vehiculo->color, ['class' => 'form-control' . ($errors->has('color') ? ' is-invalid' : ''), 'placeholder' => 'Color']) }}
                                        {!! $errors->first('color', '<div class="invalid-feedback">:message</p>') !!}
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de Combustible</label>
                                        <select class="form-control" name="tipo_combustible" id="tipo_combustible">
                                            <option value="1">Gasolina</option>
                                            <option value="2">Diesel</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Adquisición</label>
                                        {{ Form::text('fecha_adquisicion', $vehiculo->fecha_adquisicion, ['class' => 'form-control' . ($errors->has('fecha_adquisicion') ? ' is-invalid' : ''), 'placeholder' => 'Fecha de Adquisición']) }}
                                        {!! $errors->first('fecha_adquisicion', '<div class="invalid-feedback">:message</p>') !!}
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de Alta</label>
                                        <input class="form-control" type="text" value="{{ $vehiculo->created_at}}"readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Estatus</label>
                                        <select class="form-control" name="estatus" id="estatus" onchange="changeStatus()">
                                            <option value="1">Enviado</option>
                                            <option value="2">Asignado</option>
                                            <option value="3">Disponible</option>
                                            <option value="4">Baja</option>
                                        </select>
                                    </div>
                                         
                                    <div class="form-group" id ="empleados" style="display:none">
                                        <label>Asignar a un Empleado</label>
                                        <select class="form-control" name="user_id" id="user_id" >
                                            <option value="0">Elige un empleado</option>
                                            @foreach ($users as $user)
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    

                                </div>
                                <div class="box-footer mt20 float-right">
                                    <br>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <a href="{{ route('vehiculos.index') }}" class="btn btn-light"  data-placement="left">Cancelar</a>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        function changeStatus() {
            if(document.getElementById("estatus").value==2){
                document.getElementById("empleados").style.display='inline';
            }
            else{
                document.getElementById("user_id").value=0;
                document.getElementById("empleados").style.display="none";
            }
        }

        function load() {
            var tc= '<?= $vehiculo->tipo_combustible?>';
            document.getElementById("tipo_combustible").value = tc;
            var e= '<?= $vehiculo->estatus?>';
            document.getElementById("estatus").value = e;
            var emp= '<?= $vehiculo->user_id?>';
            document.getElementById("user_id").value = emp;
            if(document.getElementById("user_id").value > 0)
            document.getElementById("asignacion").innerHTML = "Vehículo asignado a: " + document.getElementById("user_id").options[document.getElementById("user_id").selectedIndex].text;
            else
            document.getElementById("asignacion").innerHTML = "Vehículo sin asignar" 
            
            
            if(document.getElementById("estatus").value==2){
                document.getElementById("empleados").style.display='inline';
            }
        }
        window.onload = load;

    </script>

    @endsection
